Debido a que cada una de las estaciones tiene un identificador único (nombre), se puede hacer el
almacenamiento de estaciones adyacentes a través de un HashMap teniendo como llave el nombre. Esto se hace
para efectos de rendimiento pues se tiene un indexamiento relativamente rápido dependiendo de la función
de hash implementada. 