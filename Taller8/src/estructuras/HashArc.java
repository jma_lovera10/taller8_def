package estructuras;

import java.util.ArrayList;
import java.util.List;

public class HashArc <K extends Integer ,V extends Arco>{

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoArc<Integer, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecked")
	public HashArc(){
		this(500000,5);
	}

	@SuppressWarnings("unchecked")
	public HashArc(int capacidad, float factorCargaMax) {
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		factorCarga = 0;
		tabla = new NodoArc[capacidad];
	}

	public boolean put(int llave, V valor){
		int pos = hash(llave);
		NodoArc<Integer, V> nodo = tabla[pos];
		if(nodo==null) tabla[pos] = new NodoArc<Integer, V>(llave, valor);
		else{
			while(nodo!=null){
				if(nodo.getLlave().compareTo(llave)==0){
					while(nodo.getChild()!=null){
						nodo = nodo.getChild();
					}
					nodo.setChild(new NodoArc<Integer,V>(llave,valor));
					break;
				}
				if(nodo.getNext()==null){
					nodo.setNext(new NodoArc<Integer, V>(llave, valor));
					break;
				}
				nodo = nodo.getNext();
			}
		}
		count++;
		actualizarFactor();
		return true;
	}

	public Arco delete(int inicio, int fin) {
		int pos = hash(inicio);
		V elem = null;
		NodoArc<Integer, V> actual = tabla[pos];
		NodoArc<Integer, V> actual2 = null;
		if(actual == null) return elem;
		else if(actual.getValor().darNodoInicio().darId()==inicio && actual.getValor().darNodoFin().darId()==fin){
			if(actual.getChild()==null){
				tabla[pos]= actual.getNext();
			}else{
				actual.getChild().setNext(actual.getNext());
				tabla[pos] = actual.getChild();
			}
			elem = actual.getValor();
		}
		else {
			while(actual.getNext()!=null){
				if(actual.getNext().getLlave()==inicio){
					if(actual.getNext().getValor().darNodoFin().darId()==fin){
						elem = actual.getNext().getValor();
						if(actual.getNext().getChild()==null){
							actual.setNext(actual.getNext().getNext());
						}else{
							actual.getNext().getChild().setNext(actual.getNext().getNext());
							actual.setNext(actual.getNext().getChild());
						}
						break;
					}
					actual2 = actual.getNext();
					while(actual2.getChild()!=null){
						if(actual2.getChild().getValor().darNodoFin().darId()==fin){
							elem = actual2.getChild().getValor();
							actual2.setChild(actual2.getChild().getChild());
						}
						actual2 = actual2.getChild();
					}
				}
				actual = actual.getNext();
			}
		}

		pos = hash(fin);
		actual = tabla[pos];

		if(actual == null) return elem;
		else if(actual.getValor().darNodoInicio().darId()==inicio && actual.getValor().darNodoFin().darId()==fin){
			if(actual.getChild()==null){
				tabla[pos]= actual.getNext();
			}else{
				actual.getChild().setNext(actual.getNext());
				tabla[pos] = actual.getChild();
			}
			elem = actual.getValor();
		}
		else {
			while(actual.getNext()!=null){
				if(actual.getNext().getLlave()==fin){
					if(actual.getNext().getValor().darNodoInicio().darId()==inicio){
						elem = actual.getNext().getValor();
						if(actual.getNext().getChild()==null){
							actual.setNext(actual.getNext().getNext());
						}else{
							actual.getNext().getChild().setNext(actual.getNext().getNext());
							actual.setNext(actual.getNext().getChild());
						}
						break;
					}
					actual2 = actual.getNext();
					while(actual2.getChild()!=null){
						if(actual2.getChild().getValor().darNodoInicio().darId()==inicio){
							elem = actual2.getChild().getValor();
							actual2.setChild(actual2.getChild().getChild());
						}
						actual2 = actual2.getChild();
					}
				}
				actual = actual.getNext();
			}
		}

		return elem;
	}

	private void actualizarFactor() {
		factorCarga = count/capacidad;
	}

	public V[] values(){
		V[] valores = (V[]) new Arco[count];
		int i = 0;
		for (NodoArc<Integer,V> v : tabla) {
			while(v!=null){
				valores[i++] = v.getValor();
				v = v.getNext();
			}
		}
		return valores;
	}

	public Arco[] darArcosOrigen(int id) {

		int pos = hash(id);
		NodoArc<Integer, V> actual = tabla[pos];
		NodoArc<Integer, V> actual2 = null;
		List<Arco> lista = new ArrayList<Arco>();
		while(actual!=null){
			if(actual.getValor().darNodoInicio().darId()==id){
				actual2 = actual;
				while(actual2!=null){
					lista.add(actual2.getValor());
					actual2 = actual2.getChild();
				}
				break;
			}
			actual = actual.getNext();
		}
		Object[] arr = lista.toArray();
		Arco[] resp = new Arco[arr.length];

		for (int i = 0; i < resp.length; i++) {
			resp[i] = (Arco) arr[i];
		}

		return resp;
	}


	public Arco[] darArcosDestino(int id) {

		int pos = hash(id);
		NodoArc<Integer, V> actual = tabla[pos];
		NodoArc<Integer, V> actual2 = null;
		List<Arco> lista = new ArrayList<Arco>();
		while(actual!=null){
			if(actual.getValor().darNodoFin().darId()==id){
				actual2 = actual;
				while(actual2!=null){
					lista.add(actual2.getValor());
					actual2 = actual2.getChild();
				}
				break;
			}
			actual = actual.getNext();
		}
		Object[] arr = lista.toArray();
		Arco[] resp = new Arco[arr.length];

		for (int i = 0; i < resp.length; i++) {
			resp[i] = (Arco) arr[i];
		}

		return resp;
	}

	public int size(){
		return count;
	}
	
	public boolean contains(int key , V elem){
		int pos = hash(key);
		NodoArc<Integer, V> nodo = tabla[pos];
		NodoArc<Integer, V> actual = null;
		while(nodo!=null){
			if(nodo.getValor().darNodoFin().darId()==elem.darNodoFin().darId()&&nodo.getValor().darNodoInicio().darId()==elem.darNodoInicio().darId()){
				return true;
			}
			else{
				actual = nodo.getChild();
				while(actual!=null){
					if(nodo.getValor().darNodoFin().darId()==elem.darNodoFin().darId()&&nodo.getValor().darNodoInicio().darId()==elem.darNodoInicio().darId()){
						return true;
					}
					actual = actual.getChild();
				}
			}
			nodo = nodo.getNext();
		}
		return false;
	}
	
	//Hash
	private int hash(Integer llave)
	{
		return llave.hashCode()%capacidad;
	}
}
