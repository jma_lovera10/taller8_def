package estructuras;

public class TablaHash<Integer ,V> {

	/**
	 * Factor de carga actual de la tabla (porcentaje utilizado).
	 */
	private float factorCarga;

	/**
	 * Factor de carga maximo que soporta la tabla.
	 */
	private float factorCargaMax;

	/**
	 * Estructura que soporta la tabla.
	 */
	private NodoHash<Integer, V>[] tabla;

	/**
	 * La cuenta de elementos actuales.
	 */
	private int count;

	/**
	 * La capacidad actual de la tabla. Tamaño del arreglo fijo.
	 */
	private int capacidad;

	//Constructores

	@SuppressWarnings("unchecIntegered")
	public TablaHash(){
		this(5000,5);
	}

	@SuppressWarnings("unchecIntegered")
	public TablaHash(int capacidad, float factorCargaMax) {
		this.factorCargaMax = factorCargaMax;
		this.capacidad = capacidad;
		count = 0;
		factorCarga = 0;
		tabla = new NodoHash[capacidad];
	}

	public boolean put(Integer llave, V valor){

		int pos = hash(llave);
		NodoHash<Integer, V> nodo = tabla[pos];
		if(nodo==null) tabla[pos] = new NodoHash<Integer, V>(llave, valor);
		else{
			if(nodo.getLlave()==llave){
				nodo.setValor(valor);
				return true;
			}
			while(nodo.getNext()!=null){
				if(nodo.getNext().getLlave()==llave){
					nodo.getNext().setValor(valor);
					return true;
				}
				nodo = nodo.getNext();
			}
			nodo.setNext(new NodoHash<Integer, V>(llave, valor));
		}
		count++;
		actualizarFactor();
		return true;
	}

	public V get(Integer llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<Integer, V> actual = tabla[pos];
		while(actual!=null){
			if(actual.getLlave()==llave){
				elem = actual.getValor();
				break;
			}
			actual = actual.getNext();
		}

		return elem;
	}

	public V delete(Integer llave){
		int pos = hash(llave);
		V elem = null;
		NodoHash<Integer, V> actual = tabla[pos];
		if(actual == null) return elem;
		else if(actual.getLlave()==llave){
			elem =actual.getValor();
			tabla[pos] = tabla[pos].getNext();
		}
		else{
			while(actual.getNext()!=null){
				if(actual.getNext().getLlave()==llave){
					elem = actual.getNext().getValor();
					actual.setNext(actual.getNext().getNext());
					count--;
					actualizarFactor();
					break;
				}
				actual = actual.getNext();
			}
		}

		return elem;
	}

	private void actualizarFactor() {
		factorCarga = count/capacidad;
	}

	public V[] values(){
		V[] valores = (V[]) new Object[count];
		int i = 0;
		for (NodoHash<Integer,V> v : tabla) {
			while(v!=null){
				valores[i++] = v.getValor();
				v = v.getNext();
			}
		}
		return valores;
	}

	public int size(){
		return count;
	}
	//Hash
	private int hash(Integer llave)
	{
		return llave.hashCode()%capacidad;
	}

}