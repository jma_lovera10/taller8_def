package estructuras;

public class NodoArc<K extends Comparable<K>,V> {

	private K llave;
	private V valor;
	private NodoArc<K, V> next;
	private NodoArc<K, V> child;
	
	public NodoArc(K llave, V valor) {
		super();
		this.llave = llave;
		this.valor = valor;
		this.next = null;
		this.child = null;
	}
	
	public K getLlave() {
		return llave;
	}
	
	public void setLlave(K llave) {
		this.llave = llave;
	}
	
	public V getValor() {
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public NodoArc<K, V> getNext(){
		return this.next;
	}
	
	public NodoArc<K, V> getChild(){
		return this.child;
	}
	
	public void setNext(NodoArc<K, V> next){
		this.next = next;
	}
	
	public void setChild(NodoArc<K, V> child){
		this.child = child;
	}
}
