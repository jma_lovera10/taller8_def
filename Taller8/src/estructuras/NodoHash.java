package estructuras;

public class NodoHash<Integer,V> {

	private Integer llave;
	private V valor;
	private NodoHash<Integer, V> next;
	
	public NodoHash(Integer llave, V valor) {
		super();
		this.llave = llave;
		this.valor = valor;
		this.next = null;
	}
	
	public Integer getLlave() {
		return llave;
	}
	
	public void setLlave(Integer llave) {
		this.llave = llave;
	}
	
	public V getValor() {
		return valor;
	}
	
	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public NodoHash<Integer, V> getNext(){
		return this.next;
	}
	
	public void setNext(NodoHash<Integer, V> next){
		this.next = next;
	}
}
