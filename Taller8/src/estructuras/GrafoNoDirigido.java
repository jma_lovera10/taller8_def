package estructuras;

/**
 * Clase que representa un grafo con peso no dirigido.
 * @author SamuelSalazar
 * @param <T> El tipo que se va a utilizar como nodos del grafo
 */
public class GrafoNoDirigido<T extends Nodo> implements Grafo<T> {

	/**
	 * Nodos del grafo
	 */
	private TablaHash<Integer, T> nodos; 

	/**
	 * Lista de adyacencia 
	 */
	private HashArc<Integer,Arco> adj;

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		nodos = new TablaHash<Integer,T>();
		adj = new HashArc<Integer,Arco>();
	}

	@Override
	public boolean agregarNodo(T nodo) {
		return nodos.put(nodo.hashCode(), nodo);
	}

	@Override
	public boolean eliminarNodo(int id) {
		return nodos.delete(id)!=null;
	}

	@Override
	public Arco[] darArcos() {
		return adj.values();
	}

	private <E extends Comparable<E>> Arco crearArco( final int inicio, final int fin, final double costo, E e )
	{
		return new Arco(buscarNodo(inicio), buscarNodo(fin), costo, e);
	}

	@Override
	public Nodo[] darNodos() {
		Object[] n = nodos.values();
		Nodo[] m = new Nodo[n.length];
		for (int i = 0; i < m.length; i++) {
			m[i] = (Nodo)n[i];
		}
		return m;
	}

	@Override
	public <E extends Comparable<E>> boolean agregarArco(int i, int f, double costo, E obj) {
		Arco arc = crearArco(i, f, costo, obj);
		boolean resp = adj.put(i, arc);
		resp &= adj.put(f, arc);
		return resp;
	}

	@Override
	public boolean agregarArco(int i, int f, double costo) {
		return agregarArco(i, f, costo, null);
	}

	@Override
	public Arco eliminarArco(int inicio, int fin) {
		return adj.delete(inicio,fin);
	}

	@Override
	public Nodo buscarNodo(int id) {
		return nodos.get(id);
	}

	@Override
	public Arco[] darArcosOrigen(int id) {
		return adj.darArcosOrigen(id);
	}

	@Override
	public Arco[] darArcosDestino(int id) {
		return adj.darArcosDestino(id);
	}
}
